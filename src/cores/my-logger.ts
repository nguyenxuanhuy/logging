import { Injectable, Scope, Logger } from '@nestjs/common';

@Injectable({ scope: Scope.TRANSIENT })
export class MyLogger extends Logger {
  throwError(error: Error): void {
    this.error(error);
  }

  static throwError(error: Error): void {
    if (error instanceof Error) {
      return super.error(error.message);
    }
    if (typeof error === 'object') return super.error(JSON.stringify(error));
    super.error(error);
  }

  debug(message: any, ...optionalParams: [...any, string?, string?]) {
    if (message instanceof Error) {
      return super.warn(message.message, optionalParams);
    }
    if (typeof message === 'object')
      return super.warn(JSON.stringify(message), optionalParams);
    super.warn(message, optionalParams);
  }

  static log(message: any, ...optionalParams: [...any, string?, string?]) {
    if (message instanceof Error) {
      return super.warn(message.message, optionalParams);
    }
    if (typeof message === 'object')
      return super.warn(JSON.stringify(message), optionalParams);
    super.warn(message, optionalParams);
  }

  log(message: any, ...optionalParams: [...any, string?, string?]) {
    if (message instanceof Error) {
      return super.warn(message.message, optionalParams);
    }
    if (typeof message === 'object')
      return super.warn(JSON.stringify(message), optionalParams);
    super.warn(message, optionalParams);
  }

  warn(message: any, ...optionalParams: [...any, string?, string?]) {
    if (message instanceof Error) {
      return super.warn(message.message, optionalParams);
    }

    if (typeof message === 'object')
      return super.warn(JSON.stringify(message), optionalParams);

    super.warn(message, optionalParams);
  }

  error(message: any, ...optionalParams: [...any, string?, string?]) {
    if (message instanceof Error) {
      return super.error(message.message, optionalParams);
    }

    if (typeof message === 'object')
      return super.warn(JSON.stringify(message), optionalParams);
    super.error(message, optionalParams);
  }

  setContext(context: string) {
    super.context = context;
  }

  resetContext() {
    super.context = null;
  }
}
