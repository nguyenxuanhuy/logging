import { Logger } from '@nestjs/common';

export class BaseLogger extends Logger {
  throwError(error: Error): void {
    this.error(error);
  }

  static throwError(error: Error): void {
    this.error(error);
  }

  debug(message: any, ...optionalParams: [...any, string?, string?]) {
    super.debug(message, optionalParams);
  }

  static log(message: any, ...optionalParams: [...any, string?, string?]) {
    super.warn(message, optionalParams);
  }

  log(message: any, ...optionalParams: [...any, string?, string?]) {
    super.warn(message, optionalParams);
  }

  warn(message: any, ...optionalParams: [...any, string?, string?]) {
    super.warn(message, optionalParams);
  }

  error(message: any, ...optionalParams: [...any, string?, string?]) {
    super.error(message, optionalParams);
  }

  setContext(context: string) {
    super.context = context;
  }

  resetContext() {
    super.context = null;
  }
}
