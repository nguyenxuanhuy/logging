import * as winston from 'winston';
import DailyRotateFile = require('winston-daily-rotate-file');

const { printf } = winston.format;

const getContextName = (info) => {
  delete info.level;
  delete info.message;
  delete info.timestamp;
  delete info.ms;
  let name = '';
  Object.keys(info).forEach((key) => {
    if (!isNaN(+key)) {
      name += info[key];
    }
  });
  if (!name) name = 'App';
  return name;
};

const myFormat = printf((info) => {
  if (info.name)
    return `[${info.timestamp}] [${info.name} - (${info.level})]: ${info.message}`;
  return JSON.stringify(info);
});

const errorStackTracerFormat = winston.format((info) => {
  const label = getContextName({ ...info });

  const error = info.message as any;
  if (typeof error === 'object' && error.message) {
    const newInfo = {
      timestamp: info.timestamp || new Date().toISOString(),
      level: info.level,
      name: label,
      message: JSON.stringify(error),
    };
    return newInfo;
  }
  const logInfo = {
    timestamp: info.timestamp || new Date().toISOString(),
    level: info.level || 'info',
    name: label,
    message: info.message || info.response?.message || '',
  };
  return logInfo;
});

const combinedFormat = winston.format.combine(
  winston.format.timestamp({ format: 'YYYY-MM-DD HH:mm:ss' }),
  winston.format.colorize({
    all: true,
  }),
  winston.format.ms(),
  winston.format.splat(), // Necessary to produce the 'meta' property
  errorStackTracerFormat(),
  myFormat,
);

const transport = new DailyRotateFile({
  filename: './logs/%DATE%',
  datePattern: 'YYYY/MM/DD',
  extension: '.log',
  maxSize: '500m',
  format: combinedFormat,
});

export function createLogger() {
  const logger = winston.createLogger({
    transports: [
      transport,
      new winston.transports.Console({
        format: combinedFormat,
      }),
    ],
  });

  return logger;
}
