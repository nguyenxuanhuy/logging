"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BaseLogger = void 0;
const common_1 = require("@nestjs/common");
class BaseLogger extends common_1.Logger {
    throwError(error) {
        this.error(error);
    }
    static throwError(error) {
        this.error(error);
    }
    debug(message, ...optionalParams) {
        super.debug(message, optionalParams);
    }
    static log(message, ...optionalParams) {
        super.warn(message, optionalParams);
    }
    log(message, ...optionalParams) {
        super.warn(message, optionalParams);
    }
    warn(message, ...optionalParams) {
        super.warn(message, optionalParams);
    }
    error(message, ...optionalParams) {
        super.error(message, optionalParams);
    }
    setContext(context) {
        super.context = context;
    }
    resetContext() {
        super.context = null;
    }
}
exports.BaseLogger = BaseLogger;
//# sourceMappingURL=base-logger.js.map