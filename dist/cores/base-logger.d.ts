import { Logger } from '@nestjs/common';
export declare class BaseLogger extends Logger {
    throwError(error: Error): void;
    static throwError(error: Error): void;
    debug(message: any, ...optionalParams: [...any, string?, string?]): void;
    static log(message: any, ...optionalParams: [...any, string?, string?]): void;
    log(message: any, ...optionalParams: [...any, string?, string?]): void;
    warn(message: any, ...optionalParams: [...any, string?, string?]): void;
    error(message: any, ...optionalParams: [...any, string?, string?]): void;
    setContext(context: string): void;
    resetContext(): void;
}
