"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MyLogger = void 0;
const common_1 = require("@nestjs/common");
let MyLogger = class MyLogger extends common_1.Logger {
    throwError(error) {
        this.error(error);
    }
    static throwError(error) {
        if (error instanceof Error) {
            return super.error(error.message);
        }
        if (typeof error === 'object')
            return super.error(JSON.stringify(error));
        super.error(error);
    }
    debug(message, ...optionalParams) {
        if (message instanceof Error) {
            return super.warn(message.message, optionalParams);
        }
        if (typeof message === 'object')
            return super.warn(JSON.stringify(message), optionalParams);
        super.warn(message, optionalParams);
    }
    static log(message, ...optionalParams) {
        if (message instanceof Error) {
            return super.warn(message.message, optionalParams);
        }
        if (typeof message === 'object')
            return super.warn(JSON.stringify(message), optionalParams);
        super.warn(message, optionalParams);
    }
    log(message, ...optionalParams) {
        if (message instanceof Error) {
            return super.warn(message.message, optionalParams);
        }
        if (typeof message === 'object')
            return super.warn(JSON.stringify(message), optionalParams);
        super.warn(message, optionalParams);
    }
    warn(message, ...optionalParams) {
        if (message instanceof Error) {
            return super.warn(message.message, optionalParams);
        }
        if (typeof message === 'object')
            return super.warn(JSON.stringify(message), optionalParams);
        super.warn(message, optionalParams);
    }
    error(message, ...optionalParams) {
        if (message instanceof Error) {
            return super.error(message.message, optionalParams);
        }
        if (typeof message === 'object')
            return super.warn(JSON.stringify(message), optionalParams);
        super.error(message, optionalParams);
    }
    setContext(context) {
        super.context = context;
    }
    resetContext() {
        super.context = null;
    }
};
MyLogger = __decorate([
    (0, common_1.Injectable)({ scope: common_1.Scope.TRANSIENT })
], MyLogger);
exports.MyLogger = MyLogger;
//# sourceMappingURL=my-logger.js.map